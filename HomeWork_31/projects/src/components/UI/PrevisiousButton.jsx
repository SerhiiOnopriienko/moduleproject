import React, { useContext } from "react";
import Button from '@mui/material/Button';
import { numContext } from "../Context/Context";

export default function PrevisiousButton () {
    let [num, setNum] = useContext(numContext);
    return (
            <Button 
            onClick={() => setNum(--num)}
            type="button"
            variant="outlined" 
            color="primary" 
            >
            Previsious Step
            </Button>
    )
}
