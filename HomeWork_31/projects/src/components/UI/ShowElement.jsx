import React, { useContext } from "react";
import BasicInformation from "../BasicInformation";
import Password from "../Password";
import Conclusion from '../Conclusion';
import { numContext } from "../Context/Context";

export default function ShowElement(){
  let [num, setNum] = useContext(numContext);

    if(num===1){
      return <BasicInformation />
    } else if(num===2){
      return <Password />
    } 
    return <Conclusion />
  }