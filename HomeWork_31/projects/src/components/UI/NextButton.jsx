import React from "react";
import Button from '@mui/material/Button';

export default function NextButton (props) {
    return (
            <Button 
            {...props}
            type="submit"
            variant="outlined" 
            color="primary" 
            >
            
            </Button>
    )
}
