import React, { createContext, useReducer, useState } from "react";
import { formReducer, initialState } from '../Reducer/Reducer';

export const FormContext = createContext({});
export const numContext = createContext({});

export default function FormContextComponent ({children}) {
    const [state, dispatch] = useReducer(formReducer, initialState);
    const [num, setNum] = useState(1);
    return (
        <FormContext.Provider value={[state, dispatch]} >
            <numContext.Provider value={[num, setNum]}>
                {children}  
            </numContext.Provider>   
        </FormContext.Provider>
    )
}