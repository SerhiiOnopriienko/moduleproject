/**
 * @typedef {Object} Action
 * @property {string} type
 * @property {*} payload
 */
export const initialState = {
    conclusionData: [],
}

const SHOW_CONCLUSION = '[CONCLUSION] Show Conclusion Data';

// actions
/**
 * @param {Array} conclusionData - Array of conclusion data
 * @param {string} conclusionData.firstName - user`s first name
 * @param {string} conclusionData.surname - user`s surname
 * @param {string} conclusionData.password - user`s password
 * @returns {Object} action
 */

export const showConclusion = (conclusionData) => ({
    type: SHOW_CONCLUSION,
    payload: { conclusionData }
});


export const formReducer = (state = initialState, action) => {
    switch(action.type){
        case SHOW_CONCLUSION:
            return {
                ...state,
                conclusionData: [...state.conclusionData, action.payload.conclusionData],
            };
        default:
            return state;
    }}
