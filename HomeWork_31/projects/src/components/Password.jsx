import React, { useContext } from "react";
import Box from '@mui/material/Box';
import Input from "./UI/Input";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { FormContext } from './Context/Context';
import { showConclusion } from './Reducer/Reducer';
import * as yup from "yup";
import PrevisiousButton from "./UI/PrevisiousButton";
import NextButton from "./UI/NextButton";
import { numContext } from "./Context/Context";

const userSchema = yup.object({
  password: yup.string().required(),
  confirmPassword: yup.string().required()
  .oneOf([yup.ref("password")], "Passwords do not match"),
  });

export default function Password () {
  let [{conclusionData}, dispatch] = useContext(FormContext);
  let [num, setNum] = useContext(numContext);
  const { register, handleSubmit, formState: { errors } } = useForm({resolver: yupResolver(userSchema)});

  const onSubmit = (conclusionData) => {
    setNum(++num);
    dispatch(showConclusion(conclusionData));
  } 
    return (
        <>
        <h1>Password</h1>
        <Box
          onSubmit={handleSubmit(onSubmit)}
          component="form"
          noValidate
          autoComplete="off"
        >
        <Input 
          defaultValue={conclusionData.length > 1 ? conclusionData[1].password : ''}
          {...register('password')} 
          type='password' 
          label='Choose password' 
        />
        <p style={{color: 'red'}}>{errors.password?.message}</p>
        <Input 
          defaultValue={conclusionData.length > 1 ? conclusionData[1].confirmPassword : ''}
          {...register('confirmPassword')} 
          type='password' 
          label='Confirm password' 
        />
        <p style={{color: 'red'}}>{errors.confirmPassword?.message}</p>
        <div>
          <PrevisiousButton />
          <NextButton sx={{ml: 29.5}}>Next Step</NextButton>
        </div>       
    </Box>
        </>
    )
}