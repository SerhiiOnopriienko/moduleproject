import React, { useContext, useState } from "react";
import { FormContext } from './Context/Context';
import PrevisiousButton from "./UI/PrevisiousButton";
import NextButton from "./UI/NextButton";
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

export default function Conclusion () {
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [{conclusionData}] = useContext(FormContext);
    const greettings = `Dear ${conclusionData[0].firstName} ${conclusionData[0].surname}, welcome to our space, verification link was sent on your email ${conclusionData[0].email}, your password is ${conclusionData[1].password}.`
    return (
        <>
        <h1>Conclusion</h1>
        <div>
            <p><b>First name:</b> {conclusionData[0].firstName}</p>
            <p><b>Surname:</b> {conclusionData[0].surname}</p>
            <p><b>E-mail:</b> {conclusionData[0].email}</p>
            <p><b>Password: </b>{conclusionData[1].password}</p>
        </div>
        <div>
            <PrevisiousButton />
            <NextButton onClick={handleOpen} sx={{ml: 29.5}}>Finish</NextButton>
        </div>
        <div>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Congratulations!
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                    {greettings}
                </Typography>
                </Box>
            </Modal>
        </div>
        </>
    )
}