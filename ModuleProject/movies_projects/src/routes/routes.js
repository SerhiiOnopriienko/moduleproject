import { Navigate, createBrowserRouter } from "react-router-dom";
import ProtectedRouter from "./ProtectedRouter";
import Wrapper from "../pages/Wrapper";
import Login from "../pages/Authentication/Login";
import Register from "../pages/Authentication/Register";
import MovieCard from "../pages/MovieCard";
import MainContent from "../Components/MainContent/MainContent";
import Favorites from "../pages/Favorites";
import Search from "../pages/Search";
import FilteredMovies from "../pages/FilteredMovies";

export const router = createBrowserRouter([
  {
    path: "/main",
    element: (
      <ProtectedRouter>
        <Wrapper />
      </ProtectedRouter>
    ),
    children: [
      {
        path: "/main",
        element: <MainContent />,
      },
      {
        path: "/main/:movieId",
        element: <MovieCard />,
      },
      {
        path: "main/favorites",
        element: <Favorites />,
        children: [
          {
            path: "main/main/favorites/:movieId",
            element: <MovieCard />,
          },
        ],
      },
      {
        path: "/main/search",
        element: <Search />,
      },
      {
        path: "/main/filtered",
        element: <FilteredMovies />,
      },
      {
        path: "*",
        element: <h1>Not found</h1>,
      },
    ],
  },
  {
    path: "login",
    element: <Login />,
  },
  {
    path: "register",
    element: <Register />,
  },
  {
    path: "*",
    element: <Navigate to="/main" />,
  },
]);
