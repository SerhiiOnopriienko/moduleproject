//Task 1
let age = +prompt('How old are you?', 31);
alert(age);

//Task 2
let name = prompt('What is your name?', 'Serhii');
let city = prompt('What city do you live in?', 'Kyiv');
let country = prompt('What country do you live in?', 'Ukraine');
console.log(`Hello, your name is ${name}. You live in ${city}, ${country}.`);

//Task 3
let number = 156798;
let string = 'IT School Hillel';
let boolean = false;
console.log(`Value: ${number}; type: ${typeof number}.`);
console.log(`Value: ${string}; type: ${typeof string}.`);
console.log(`Value: ${boolean}; type: ${typeof boolean}.`);